/*!
 * \file:	main.c
 * \author:	Vojtech Vigner, vojtech.vigner@gmail.com
 * \date:	2020-10-02
 * \brief:	Main application code.
 *
 * \attention The MIT License (MIT)
 * Copyright (c) 2020 Vojtech Vigner, vojtech.vigner@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "stm32h7xx_ll_gpio.h"
#include "stm32h7xx_ll_bus.h"
#include "stm32h7xx_ll_utils.h"
#include "stm32h7xx_ll_cortex.h"

/* Nucleo board definitions */
#include "nucleo-h743zix.h"

/**
 * @brief Main loop, runs forever.
 * 
 * @return int function should never return
 */
int main(void) {
    /* System clock is set in `SystemInit` function which is called before `main`. */
    /* This is done via startup script. Function `SystemInit` configures clock to  */
    /* 64 MHz using HSI - internal oscillator                                      */

    /* Enable clock to GPIO peripheral */
    LED1_GPIO_CLK_ENABLE();
    LED3_GPIO_CLK_ENABLE();

    /* Init LED_PINx as output */
    LL_GPIO_SetPinMode(LED1_GPIO_PORT, LED1_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinMode(LED3_GPIO_PORT, LED3_PIN, LL_GPIO_MODE_OUTPUT);

    /* Update SystemCoreClock value, should be 64000000 */
    SystemCoreClockUpdate();

    /* Start SysTick timer with 1 ms period */
    /* This is necessary in order to use LL_mDelay function */
    LL_Init1msTick(SystemCoreClock);

    /* Enable SysTick Timer interrupt */
    LL_SYSTICK_EnableIT();

    while (1) {
        /* Toggle LED_PIN */
        LL_GPIO_TogglePin(LED1_GPIO_PORT, LED1_PIN);

        /* Wait 200 ms */
        LL_mDelay(200);
    }
    return 0;
}

/**
 * @brief SysTick Timer interrupt handler
 * 
 */
void SysTick_Handler(void) {
    static int ticks = 0;

    ticks++;

    /* Wait 200 ms */
    if (ticks > 200) {
        /* Toggle LED_PIN */
        LL_GPIO_TogglePin(LED3_GPIO_PORT, LED3_PIN);
        ticks = 0;
    }
}
