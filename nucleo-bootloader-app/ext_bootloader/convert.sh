#!/bin/bash

# Converts binary file to hex text file
# `xxd` can be installed by installing `vim`
xxd -i < bootloader.bin > bootloader_bin.def 
