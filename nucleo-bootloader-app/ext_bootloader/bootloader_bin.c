/*!
 * \file:	bootloader_bin.c
 * \author:	Vojtech Vigner, vojtech.vigner@gmail.com
 * \date:	2020-10-02
 * \brief:	Bootloader binary.
 *
 * \attention The MIT License (MIT)
 * Copyright (c) 2020 Vojtech Vigner, vojtech.vigner@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdint.h>

/* This will embed bootloader binary to application binary */
unsigned char bootloader_bin[] __attribute__((section(".bootloader")));

/*
 * bootloader_bin.def can be obtained from bootloader.bin by running convert.sh 
 * You need to have `xxd` installed, it is usually part of `vim`. 
 * If you do not like to have bootloader as part of application binary disable
 * compillation of this file by removing `src_filter = +<../ext_bootloader/bootloader_bin.c>`
 * in platformio.ini. 
 */
unsigned char bootloader_bin[] = {
#include "../ext_bootloader/bootloader_bin.def"
};
