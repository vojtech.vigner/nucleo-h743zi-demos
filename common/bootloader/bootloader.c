/*!
 * \file:	bootloader.c
 * \author:	Vojtech Vigner, vojtech.vigner@gmail.com
 * \date:	2020-10-02
 * \brief:	Bootloader functions.
 *
 * \attention The MIT License (MIT)
 * Copyright (c) 2020 Vojtech Vigner, vojtech.vigner@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <string.h>
#include <stdint.h>

#include "stm32h7xx_ll_system.h"

#include "bootloader.h"

/* Definitions from linker script */
extern uint32_t _LD_FIRMWARE;
extern uint32_t _LD_BOOTLOADER;
extern uint32_t _LD_RAM;
extern uint32_t _estack;

/* Addresses of individual sections */
#define ADDR_FIRMWARE   ((uint32_t)&_LD_FIRMWARE)
#define ADDR_RAM        ((uint32_t)&_LD_RAM)
#define ADDR_BOOTLOADER ((uint32_t)&_LD_BOOTLOADER)

/**
 * @brief  Runs application located on address ADDR_FIRMWARE.
 *         This function should never return.
 */
void BOOT_RunProgram(void) {
    __disable_irq();
    for (int i = 0; i < 8; i++)
        NVIC->ICER[i] = 0xFFFFFFFF;
    for (int i = 0; i < 8; i++)
        NVIC->ICPR[i] = 0xFFFFFFFF;
    __set_CONTROL(0);
    __set_MSP(*(__IO uint32_t *)ADDR_FIRMWARE);
    uint32_t JumpAddress = *((volatile uint32_t *)(ADDR_FIRMWARE + 4));
    __ISB();
    __DSB();
    SysTick->CTRL &= ~(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk);
    void (*reset_handler)(void) = (void *)JumpAddress;
    while (1) {
        reset_handler();
    }
}

/**
 * @brief Checks if there is a application located at ADDR_FIRMWARE.
 * 
 * @return true if application seems to be valid
 * @return false if application is invalid
 */
bool BOOT_IsProgramValid(void) {
    __IO uint32_t *ApplicationAddress = (__IO uint32_t *)ADDR_FIRMWARE;

    /* Check if SP points to RAM */
    if ((ApplicationAddress[0] <= ADDR_RAM) || (ApplicationAddress[0] > ((uint32_t)&_estack))) {
        return false;
    }

    /* Check if ResetHandler points to user flash */
    if ((ApplicationAddress[1] < ADDR_FIRMWARE) || (ApplicationAddress[1] >= FLASH_END)) {
        return false;
    }
    return true;
}

/**
 * @brief Sets Vector Table Offset Register to ADDR_FIRMWARE.
 *        This function should be used only from application.
 *        Also it should be called before any initializations.
 */
void BOOT_InitVectors(void) {
    __disable_irq();
    SCB->VTOR = ADDR_FIRMWARE;
    __DSB();
    __ISB();
    __enable_irq();
}
