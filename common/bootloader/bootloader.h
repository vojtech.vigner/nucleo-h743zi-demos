/*!
 * \file:	bootloader.h
 * \author:	Vojtech Vigner, vojtech.vigner@gmail.com
 * \date:	2020-10-02
 * \brief:	Bootloader functions.
 *
 * \attention The MIT License (MIT)
 * Copyright (c) 2020 Vojtech Vigner, vojtech.vigner@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __BOOTLOADER_H
#define __BOOTLOADER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

/* Bootloader functions */
bool BOOT_IsProgramValid(void) __attribute__((optimize("-O0")));
void BOOT_RunProgram(void) __attribute__((optimize("-O0")));

/* Application function */
void BOOT_InitVectors(void) __attribute__((optimize("-O0")));

#ifdef __cplusplus
}
#endif

#endif /* __BOOTLOADER_H */