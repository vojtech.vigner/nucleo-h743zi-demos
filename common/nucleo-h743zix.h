/*!
 * \file:	nucleo-h743zix.h
 * \author:	Vojtech Vigner, vojtech.vigner@gmail.com
 * \date:	2020-10-03
 * \brief:	NUCLEO-H743ZI and NUCLEO-H743ZI2 board definitions.
 *
 * \attention The MIT License (MIT)
 * Copyright (c) 2020 Vojtech Vigner, vojtech.vigner@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __NUCLEO_H743ZIX_H
#define __NUCLEO_H743ZIX_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "stm32h7xx.h"

#ifndef BOARD_VERSION
#warning "BOARD_VERSION is not defined, defaulting to version 2."
#define BOARD_VERSION 2
#endif

/* External crystal frequency is 8 MHz */
#define HSE_VALUE ((uint32_t)8000000UL)

/* Pin definiton, for Nucleo-H743ZIx LED1 is on pin PB0 */
#define LED1_PIN               LL_GPIO_PIN_0
#define LED1_GPIO_PORT         GPIOB
#define LED1_GPIO_CLK_ENABLE() LL_AHB4_GRP1_EnableClock(LL_AHB4_GRP1_PERIPH_GPIOB)

#if BOARD_VERSION == 1
/* Pin definiton, for Nucleo-H743ZI LED2 is on pin PB7 */
#define LED2_PIN               LL_GPIO_PIN_7
#define LED2_GPIO_PORT         GPIOB
#define LED2_GPIO_CLK_ENABLE() LL_AHB4_GRP1_EnableClock(LL_AHB4_GRP1_PERIPH_GPIOB)
#else
/* Pin definiton, for Nucleo-H743ZI2 LED2 is on pin PE1 */
#define LED2_PIN               LL_GPIO_PIN_1
#define LED2_GPIO_PORT         GPIOE
#define LED2_GPIO_CLK_ENABLE() LL_AHB4_GRP1_EnableClock(LL_AHB4_GRP1_PERIPH_GPIOE)
#endif

/* Pin definiton, for Nucleo-H743ZIx LED3 is on pin PB14 */
#define LED3_PIN               LL_GPIO_PIN_14
#define LED3_GPIO_PORT         GPIOB
#define LED3_GPIO_CLK_ENABLE() LL_AHB4_GRP1_EnableClock(LL_AHB4_GRP1_PERIPH_GPIOB)

#ifdef __cplusplus
}
#endif

#endif /* __NUCLEO_H743ZIX_H */