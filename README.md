# NUCLEO-H743ZI Demos

Demo projects for NUCLEO-H743ZI and NUCLEO-H743ZI2 boards using [platformio](https://platformio.org/).

### Peripherals examples
+ **nucleo-simple-blinky** 
This demo toggles two user LEDs on Nucleo board. LED1 is toggled in main loop and LED3 is toggled in interrupt routine.

### Bootloader example
+ **nucleo-bootloader** 
Simple bootloader that looks for application and executes it. 

+ **nucleo-bootloader-app** 
nucleo-simple-blinky version which works with bootloader 